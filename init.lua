vvvv

if _G.DEUBG then 
  require "test_env"
end

--require("mobdebug").start()

local base_path = yworld.get_modpath("minigame")
dofile(base_path .. "/global.lua")

local PhasePrepare = import("phase/phase_prepare.lua")
local Context = import("core/context.lua")

util.set_can_dig()


--创建游戏上下文
local gameid = util.get_gameid()
logger.debug("game start id[%s]", gameid)
local ctx = Context.new(gameid)
util.store_ctx(ctx)

--从准备阶段开始
local phase = PhasePrepare.new()

local last_debug = 0
function tick(dtime)  

  if ctx:is_game_over() then
     return
  end

  if os.time() - last_debug >= 20 then
    local s = string.format("round|phase|status:%s|%s|%s player|human|daemon:%s|%s|%s", ctx.round, phase.name, phase.status, ctx:get_player_count(), ctx.human_total, ctx.daemon_total)
    logger.debug(s)
    last_debug = os.time()
  end

  ctx:lasting_inc(dtime)
  ctx.phase = phase.name
  
  phase:lasting_inc(dtime)
  phase:run(ctx)
  if phase:is_finish() then
     if phase.has_next_phase() == true then
        phase = phase:get_next_phase()
        ctx:for_each_player(function(player)
           player:send_current_phase(phase.name)
        end)
     else
        if ctx.round < config.max_round then
          logger.debug("round %s finish, start next round", ctx.round)
          next_round() --下一回合
        else
          game_over()
        end
     end
  end
  
end

function next_round()
   ctx:reset() --重置上下文
   ctx:next_round()
   phase = PhasePrepare.new() --重置当前阶段为准备阶段
end

function game_over()
    local event = Event.new(EventConstant.GAME_OVER_EVENT)
    ctx:dispatch_event(event)
end


function player_join(player)
  local event = Event.new(EventConstant.PLAYER_JOIN_EVENT)
  event.player = player
  ctx:dispatch_event(event)
end

function player_leave(player)
  local event = Event.new(EventConstant.PLAYER_LEAVE_EVENT)
  event.player = player
  ctx:dispatch_event(event)
end

function player_die(player, drop_exp, killer)
  local event = Event.new(EventConstant.PLAYER_DEATH_EVENT)
  event.player = ctx.actors[player:get_player_uid()]
  if killer ~= nil then
      event.killer = ctx.actors[killer:get_player_uid()]
  end
  ctx:dispatch_event(event)
end


function receive(player, data)
    local uid = player:get_player_uid()
    local key = data.key
    local value = data.data
    api.call(ctx, uid, key, value)
end

function slowstep(time)
    local player = ctx:rand_player()
    if player ~= nil then
        game_hiido_report.slowstep(player, time)
    end
end

--drivers
yworld.register_globalstep(tick)
yworld.register_on_joinplayer(player_join)
yworld.register_on_dieplayer(player_die)
yworld.register_on_modsdata(receive)
yworld.register_on_leaveplayer(player_leave)
--yworld.registered_slowstep(slowstep)
